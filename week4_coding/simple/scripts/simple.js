function doIt(number1,number2){
    let num1Ref=document.getElementById("number1")
    let num2Ref=document.getElementById("number2")
    let num3Ref=document.getElementById("number3")
    let num1=Number(num1Ref.value)
    let num2=Number(num2Ref.value)
    let num3=Number(num3Ref.value)
    let result=num1+num2+num3
    if(result>=0){
        document.getElementById("answer").className="positive"
    }
    else{
        document.getElementById("answer").className="negative"
    }
    let ref=document.getElementById("oddeven")
    
    if(result%2===0){
        document.getElementById("oddeven").className="even"
        ref.innerHTML="even"
    }
    else{
        document.getElementById("oddeven").className="odd"
        ref.innerHTML="odd"
    }
    let outputArea1= document.getElementById("answer")
    outputArea1.innerHTML=result
    return result
    return ref
    
}