//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    let MyVar=[54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19,
-51, -17, -25];
var PositiveOdd=new Array();
var NegativeEven=new Array();
j=0;k=0
for (let i=0; i<=MyVar.length;i++){
  if(MyVar[i]>=0 && MyVar[i]%2!==0){
    
    PositiveOdd[j]=MyVar[i];j++;
  }
  else if(MyVar[i]<0 && MyVar[i]%2==0){
    NegativeEven[k]=MyVar[i];k++
  }
}
output+="Positive Odd:"+PositiveOdd+ "\n"+ "Negative Even"+NegativeEven
   
    
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here 
    let Var1=0;
let Var2=0;
let Var3=0;
let Var4=0;
let Var5=0;
let Var6=0;
for(let i=1; i<=60000;i++){
  let Die=Math.floor((Math.random() * 6) + 1)
  if (Die==1){
    Var1++
}
  else if(Die==2){
    Var2++
}
    else if(Die==3){
    Var3++
}
    else if(Die==4){
    Var4++
}
    else if(Die==5){
    Var5++
}
    else if(Die==6){
    Var6++
}
  else
    console.log("error")
}
  
output+="Frequency of die rolls"+"\n"+"1:"+Var1+"\n"+"2:"+Var2+"\n"+"3:"+Var3+"\n"+"4:"+Var4+"\n"+"5:"+Var5+"\n"+"6:"+Var6+"\n"
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here 
    let MyVar=[0,0,0,0,0,0,0];


for(let i=1; i<=60000;i++){
  let Die=Math.floor((Math.random() * 6) + 1);
  MyVar[Die]++;
}
output+="Frequency of die rolls"+"\n"+"1:"+MyVar[1]+"\n"+"2:"+MyVar[2]+"\n"+"3:"+MyVar[3]+"\n"+"4:"+MyVar[4]+"\n"+"5:"+MyVar[5]+"\n"+"6:"+MyVar[6]+"\n"
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    let MyVar=[0,0,0,0,0,0,0];


for(let i=1; i<=60000;i++){
  let Die=Math.floor((Math.random() * 6) + 1);
  MyVar[Die]++;
}
let Expected_Value=10000;

var dieRolls = {
  Frequencies: {
    1:MyVar[1],
    2:MyVar[2],
    3:MyVar[3],
    4:MyVar[4],
    5:MyVar[5],
    6:MyVar[6],
  },
  Total:60000,
  Exceptions: ""
}

for(let i=1;i<=6;i++){
  if(Math.abs(dieRolls.Frequencies[i]-Expected_Value)>=100){
    dieRolls.Exceptions+=i+" "
  }
    }
    output+="Frequency of dice rolls"+"\n"
    output+="Total rolls:"+dieRolls.Total+"\n"
for(let prop in dieRolls.Frequencies){
    output+=prop+": "+dieRolls.Frequencies[prop]+"\n"
}
    output+="Exceptions: " + dieRolls.Exceptions
    
    
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
let person = {
name: "Jane",
 income: 180000
}
let Taxown
if (person.income>=0){
  Taxown=0

  if(person.income<=37000 && person.income>18200){
    Taxown=0.19*(person.income-18200)

  }
  else if(person.income<=90000 && person.income>37000){
    Taxown=3572+0.325*(person.income-37000)

  }
    else if(person.income<=180000 && person.income>90000){
    Taxown=20797+0.37*(person.income-90000)

  }
    else if(person.income>180000 && person.income>180000){
    Taxown=54097+0.45*(person.income-180000)

  }
  
}
else
  Taxown="error income"

  
output="Jane’s income is: $" +person.income+ ", and her tax owed is: $" +Taxown
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}