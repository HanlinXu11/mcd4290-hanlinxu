//question 1

//The function that returns the addition of its two parameters should be coded as a named function
//and the other function should be coded as an anonymous function assigned to a variable. Test that
//both versions of the code give the same output.

let output1= ""
function objectToHTML(object){
  for(let prop in object){
    output1+=(prop+":" + object[prop]+"\n")

    
  }
    return output1
}

var testObj = {
 number: 1,
 string: "abc",
 array: [5, 4, 3, 2, 1],
 boolean: true
};

objectToHTML(testObj)
let outPutArea = document.getElementById("outputArea1") 
outPutArea.innerText = output1 






//question 2
//The function that returns the addition of its two parameters should be coded as a named function
//and the other function should be coded as an anonymous function assigned to a variable. Test that
//both versions of the code give the same output

var outputAreaRef = document.getElementById("outputArea2");
var output2 = "";
function flexible(fOperation, operand1, operand2)
{
 var result = fOperation(operand1, operand2);
 return result;
}
let fc1=function(operand1, operand2){
  result=operand1+operand2
  return result
}
let fc2=function MultipleVar(operand1, operand2){
  result=operand1*operand2
  return result
}

output2 += flexible(fc1,3,5) + "\n";
output2 += flexible(fc2,3,5) + "\n";
outputAreaRef.innerText = output2;







//question 3

//var output=[] (make the final result to be a array for max and min)
//function functionname(){
//var the max and min=array[0]
//for(i=1 1<lengthofarray i++){
//if(array[i]>max){
//max=array[i]
//else if(array[i]<mini){
//mini=array[i]
//}
//ouput[0]=max;output[1]=mini}
//return output}
//






//question 4(3)
var output4=[]


function MaxMinFunction(Array){
  
  let Maxvalue=Array[0];
  let Minivalue=Array[0]
  for(let i=1;i<Array.length;i++){
    if(Array[i]>Maxvalue){
      Maxvalue=Array[i]
    }
    else if(Array[i]<Minivalue){
      Minivalue=Array[i]
    }
  }
  output4[0]=Maxvalue
  output4[1]=Minivalue
  return output4

}
    
var values = [4, 3, 6, 12, 1, 3, 8];


var outPutArea4 = document.getElementById("outputArea4") 
MaxMinFunction(values)
outPutArea4.innerText = output4[0]+"\n"+output4[1]
